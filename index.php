<html>
<head>
  <?php
  $manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main; ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/images/consumer/favicon.ico">
    <title>Bixy</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/abe2ruw.css">
    <link href="style.css" rel="stylesheet"/>
    <link href="<?php echo $main->css; ?>" rel="stylesheet"/>

    <!--[if lt IE 9]>
      <script src="~/Scripts/NewLanding/html5shiv.min.js"></script>
      <script src="~/Scripts/NewLanding/respond.min.js"></script>
    <![endif]-->


</head>
<body>


<!DOCTYPE html>
<html lang="en">
<body>
    <div class="main-wraper">
        <header id="header" class="header-main clearfix">
          <div id="header-content">
            <div class="logo"><h1><a href="/business"><img src="images/logo-white.svg" alt="logo"></a></h1></div>
          </div>
        </header>

        <!-- <ul class="fixedsidemenu" id="menu">
            <li data-menuanchor="overview" class="active"><a href="#overview">overview</a></li>
            <li class="benefits" data-menuanchor="benefits"><a href="#benefits">benefits</a></li>

        </ul>
               -->


        <div id="fullpage" class="businesspage">
          <div id="page-header">
            <div class="container">
              <h1>Overview</h1>
            </div>
          </div>
            <div class="section active overview-panel" id="section0">
                <div class="overviewblock clearfix">
                    <div class="container">
                        <div class="overview-left">
                          <div class="overview-left-content">
                            <h3>Performance + Data</h3>
                            <p>“They want to control their messaging. They also are easily incentivized. They expect to be rewarded for their loyalty, for their follows or likes. They want coupons. They want to be among the first to receive updates. They want to be included in a brand’s communications efforts." - Forbes</p>
                            <div class="btn">Talk to sales</div>
                          </div>
                        </div>
                        <div class="overview-right">
                            <div class="overviewtab login-active">

                                <ul class="nav nav-tabs clearfix">
                                    <li class="active"><a data-toggle="tab" href="#login">login</a></li>
                                    <li class=""><a data-toggle="tab" href="#signup">signup</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="login" class="tab-pane fade in active">
                                        <span class="forgotpass"><a id="PasswordResetshow" data-toggle="modal" style="display:none" data-target="#PasswordReset" href="#">Reset password?</a></span>

<div class="loginblock form-description">
    <h3>Login to Bixy</h3>
    <div class="for-business-graphic">
      <img src="images/forbusiness-blue.svg"/>
    </div>
<form action="/authentication/login" class="frmLogin signinform" method="post"><input name="__RequestVerificationToken" type="hidden" value="2mhVFadlyj_GUABfoObvgyrBZqREkUHvlXn35jsjNDT_sXk4yzfVzTifW6z6pON-d1_d6StHSS6Xu_jy4o47eHhdJUi_gghsBQiFdO5bTgQ1" /><input data-val="true" data-val-required="The LoginType field is required." id="LoginType" name="LoginType" type="hidden" value="0" /><input id="greenTheme" name="greenTheme" type="hidden" value="True" /><input id="LoginType" name="LoginType" type="hidden" value="0" />
  <div class="form-group">
  <label>Email</label>
            <input class="form-control" data-val="true" data-val-email="The Email field is not a valid e-mail address." data-val-required="The Email Address field is required." id="Email" name="Email" placeholder="Email Address" type="text" value="" />
            <span style="color:red">
                <span class="field-validation-valid" data-valmsg-for="Email" data-valmsg-replace="true"></span>
            </span>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input class="form-control" data-val="true" data-val-required="The Password field is required." id="Password" name="Password" placeholder="Password" type="password" />
            <span style="color:red">
                <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
            </span>
        </div>
        <div class="form-checkbox">
            <input type="checkbox" id="staysigned" name="">
            <label for="staysigned" class="customcheckbox">Stay signed in</label>
            <span class="forgotpass"><a data-toggle="modal" data-target="#forgotpassword" href="#">Forgot password?</a></span>
        </div>
        <input type="submit" name="" class="btn" value="login">
</form></div>

                                    </div>
                                    <div id="signup" class="tab-pane fade in">
                                        <div class="signupblock form-description">
                                            <h3>Get started with Bixy</h3>
                                            <div class="for-business-graphic">
                                              <img src="images/forbusiness-blue.svg"/>
                                            </div>
<form action="/business/signup" class="signupform" method="post" name="signup"><input name="__RequestVerificationToken" type="hidden" value="-yq6McDDpGqBsTTjVaNJY_lfsf745karLY8p17SjNukxUO7CO04EiRHTf4qMugtp76izyK9e7DWN_psz4oCzaKJ-3PRCmj4cBNddMa3a-gU1" />                                                <div class="form-group">
                  <label>Full Name</label>
                                                    <input class="form-control" data-val="true" data-val-required="The Full Name field is required." id="startHere" name="Name" placeholder="Full Name" type="text" value="" />
                                                    <span style="color:red">
                                                        <span class="field-validation-valid" data-valmsg-for="Name" data-valmsg-replace="true"></span>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                  <label>Business Name</label>
                                                    <input class="form-control" data-val="true" data-val-required="The Business Name field is required." name="BusinessName" placeholder="Business Name" type="text" value="" />
                                                    <span style="color:red">
                                                        <span class="field-validation-valid" data-valmsg-for="BusinessName" data-valmsg-replace="true"></span>
                                                    </span>
                                                </div>
                                                    <div class="form-group">
                                                      <label>Email</label>
                                                        <input class="form-control" data-val="true" data-val-email="The Email Address field is not a valid e-mail address." data-val-remote="There Is Already A User With This E-mail Address." data-val-remote-additionalfields="*.EmailAddress" data-val-remote-url="/authentication/validate-new-user-email" data-val-required="The Email Address field is required." id="EmailAddress" name="EmailAddress" placeholder="Email Address" type="email" value="" />
                                                        <span style="color:red">
                                                            <span class="field-validation-valid" data-valmsg-for="EmailAddress" data-valmsg-replace="true"></span>
                                                        </span>
                                                    </div>
                                                    <div class="form-group">
                                                      <label>Password</label>
                                                        <input class="form-control" data-val="true" data-val-required="The Password field is required." id="Password" name="Password" placeholder="Password" type="password" />
                                                        <span style="color:red">
                                                            <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
                                                        </span>
                                                    </div>
                                                    <div class="form-group">
                                                      <label>Password</label>
                                                        <input class="form-control" data-val="true" data-val-equalto="Please enter the same Password again." data-val-equalto-other="*.Password" data-val-required="The Re-type Password field is required." id="ReTypePassword" name="ReTypePassword" placeholder="Re-Type Password" type="password" />
                                                        <span style="color:red">
                                                            <span class="field-validation-valid" data-valmsg-for="ReTypePassword" data-valmsg-replace="true"></span>
                                                        </span>
                                                    </div>
                                                <div class="form-checkbox">
                                                    <a href="javascript:void(0);" class="button btnSignUp"></a>
                                                    <input type="checkbox" id="iagreeto" name="" class="agreeToTerms chkSignUp" onclick="javascript: Bixy.Business.Landing.iagreeclicked();">
                                                    <label for="iagreeto" class="customcheckbox"></label>
                                                    <span>I agree to the <a data-toggle="modal" data-target="#tearmcondition" href="#">Terms & Conditions</a></span>
                                                </div>
                                                <input type="submit" name="" class="btn" value="SignUp" id="btnSignup" disabled="disabled" onclick="javascript: Bixy.Business.Landing.submitsignup();">
</form>                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--<div class="section benefits-panel" id="section1">
                <div class="benefitsblock">
                    <div class="container">
                        <h2>Benefits</h2>
                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="benefitlist benefitlist1">
                                    <li><h4>NO CONTRACT</h4></li>
                                    <li><p>Drive revenue and loyalty through targeted, reward-based ads.</p></li>
                                    <li><p>Reach the right audience with ads tailored to user interests and favorite brands.</p></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul class="benefitlist benefitlist2">
                                    <li><p>View monthly PDF reports with measurable ROI and other key metrics.</p></li>
                                    <li><p>Manage rewards and ads from your online dashboard.</p></li>
                                    <li><p>Leverage Bixy account reps to manage your account as needed.</p></li>
                                </ul>
                            </div>
                        </div>
                        <div class="downloadmaterial clearfix">
                            <h3>Download Overview Materials:</h3>
                            <ul class="downloadmateriallist">
                                <li><a href="javascript: Bixy.Business.Landing.OverViewLocalBrand();">For Local Brands</a></li>

                                <li>
                                    <a data-toggle="modal" data-target="#AddNationalBrand" href="#">For National Brands</a>



                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->




        </div>
    </div>
    <div id="tearmcondition" class="tearmconditionpopup overflowpopup modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Bixy Privacy Statement</h4>
</div>
<div class="modal-body">
    <div class="tearmcondition-text">
        <ul class="tearmcondition-list bullet-text clearfix">
            <li><a href="#privacy-types">Types of Information We Collect</a></li>
            <li><a href="#privacy-choices">Your Choices</a></li>
            <li><a href="#privacy-uses">How Bixy Uses Information</a></li>
            <li><a href="#privacy-disclosures">When and Why Bixy Discloses Information</a></li>
            <li><a href="#privacy-collection">How Bixy Collects Information</a></li>
            <li><a href="#privacy-changes">How to make changes to your Information</a></li>
            <li><a href="#privacy-assurances">Assurances</a></li>
            <li><a href="#privacy-social">Posting to online communities or social networks</a></li>
            <li><a href="#privacy-security">Data security</a></li>
            <li><a href="#privacy-third-party">Third Party Privacy Practices</a></li>
            <li><a href="#privacy-children">Children's Policy</a></li>
            <li><a href="#privacy-contact">Contact Us</a></li>
            <li><a href="#privacy-entity">Entity</a></li>
        </ul>
        <p>This Privacy Statement (“Privacy Statement”) explains how 42 Labs, LLC (d/b/a “Bixy,” “us,” “our,” and “we”) uses your information and applies to all who use our Website, mobile applications, electronic services, social networking sites, or any individual, business partner-specific, merchant-specific, city-specific, or other area-specific Web sites we offer that link to or reference this Privacy Statement (collectively, the “Site”) and when you receive electronic communications from us. This Privacy Statement also explains how information gathered on this site is used when serving third-party digital display and multimedia ads (“Ads”) to users registered through our site (“Bixy Users”) and users who have not registered with our site (“Unregistered Users”) which are collectively referenced in this Privacy Statement as users (“Users”).</p>
        <p>Please read this Privacy Statement carefully and review it periodically for the latest information about our privacy practices. By using the Site, you agree to the terms of this Privacy Statement. If you do not agree with the practices described in this Privacy Statement, please do not provide us with your personal information or interact with the Site, and block 3rd-party cookies in your browser settings.</p>
        <p>We will routinely update this Privacy Statement to clarify our practices and to reflect new or different privacy practices, such as when we add new services, functionality or features to the Site. If we make any material changes we will notify you by email (sent to the email address specified in your account) or by means of notice on the Site prior to the change becoming effective. You can determine when this Privacy Statement was last revised by referring to the date it was “Last Updated” above.</p>
        <h3 id="privacy-types">Types of Information We Collect</h3>
        <p>We will collect information, including Personal Information and Non-Identifying Information, when you interact with us, the Site, and Ads, for example when you:</p>
        <ul class="bullet-text">
            <li>access or use the Site;</li>
            <li>create an account with Bixy;</li>
            <li>open or respond to our emails;</li>
            <li>participate in surveys or rewards programs provided on behalf of, or together with, Business Partners;</li>
            <li>visit any website online that displays our Ads or content;</li>
            <li>connect or link to any Site via social networking sites;</li>
            <li>post comments to Online Communities; and</li>
            <li>provide information to our vendors</li>
        </ul>
        <p>This Privacy Statement does not apply to the collection of information in any way other than as listed above.</p>
        <h3 id="privacy-choices">Your Choices</h3>
        <p>Our goal here at Bixy is to provide our users with a personalized experience to ensure that our content and services give our users the best value possible for their time and attention. However, you can limit the information you provide to Bixy in the following ways:</p>
        <ul class="bullet-text">
            <li>You may manage how your browser handles Cookies by adjusting its privacy and security settings. Browsers are different, so refer to instructions related to your browser to learn about cookie-related and other privacy and security settings that may be available.</li>
            <li>You may manage how your mobile device and mobile browser share certain Device Data with Bixy, as well as how your mobile browser handles Cookies by adjusting the privacy and security settings on your mobile device. Please refer to instructions provided by your mobile service provider or the manufacturer of your device to learn how to adjust your settings.</li>
            <li>You may also manage the sharing of certain Personal Information with us when you connect with us through social networking platforms or applications. Please refer to the privacy policy and settings of the social networking website or application to determine how you may adjust our permissions and manage the interactivity between Bixy and your social networking account or your mobile device.</li>
        </ul>
        <p>If you wish to opt-out of receiving offers directly from our Business Partners, you can follow the opt-out instructions in the emails that they send you.</p>
        <h3 id="privacy-uses">How Bixy Uses Information</h3>
        <p>We use information collected as described in this Privacy Statement to:</p>
        <ul class="bullet-text">
            <li>Operate, maintain, and improve the Site and our services;</li>
            <li>Provide you with personalized ads and offers for products, services, and rewards from us and participating Business Partners;</li>
            <li>Facilitate and fulfill orders placed on the Site – for example, for Bixy Rewards and other goods and services, including tracking redemption;</li>
            <li>Evaluate your eligibility for certain types of offers, products or services that may be of interest to you, and analyze advertising effectiveness;</li>
            <li>Answer your questions and respond to your requests;</li>
            <li>Perform analytics and conduct customer research;</li>
            <li>Communicate and provide additional information that may be of interest to you about Bixy and our Business Partners, sometimes by combining your information with information from Other Sources;</li>
            <li>Send you reminders, technical notices, updates, security alerts, support and administrative messages, services bulletins, marketing messages, and requested information, including on behalf of Business Partners;</li>
            <li>Administer rewards, surveys, sweepstakes, contests, or other promotional activities or events sponsored by us or our Business Partners;</li>
            <li>Manage our everyday business needs, such as administration of the Site, fulfillment, analytics, fraud prevention, and enforcement of our corporate reporting obligations and Terms of Use, or to comply with the law; and to</li>
            <li>Enhance other information we have about you directly or from Other Sources to help us better understand you and determine your interests.</li>
        </ul>
        <p>We also may use information collected as described in this Privacy Statement with your consent or as otherwise required or permitted by law.</p>
        <h3 id="privacy-disclosures">When and Why Bixy Discloses Information</h3>
        <p>We may share your Personal Information as required or permitted by law:</p>
        <ul class="bullet-text">
            <li>with our Vendors to provide services for us and who are required to protect the Personal Information;</li>
            <li>to report or collect on debts owed to us or our Business Partners</li>
            <li>with relevant Business Partners;</li>
            <li>with whom we jointly offer products and services;</li>
            <li>to facilitate a direct relationship with you, including in connection with any program we administer on behalf of the Business Partner;</li>
            <li>to enable electronic communications with you as part of purchase, a sponsored reward, offer, contest, program or other activity in which you have elected to participate;</li>
            <li>to the extent you have purchased or redeemed a Bixy Reward, goods or services offered by a Business Partner or participated in an offer, rewards, contest or other activity or program sponsored or offered through Bixy on behalf of that Business Partner;</li>
            <li>with a purchaser of Bixy or any of the Bixy Affiliates (or their assets);</li>
            <li>to comply with legal orders and government requests, or as needed to support auditing, compliance, and corporate governance functions;</li>
            <li>to combat fraud or criminal activity, and to protect our rights or those of our Affiliates, users, and Business Partners, or as part of legal proceedings affecting Bixy;</li>
            <li>in response to a subpoena, or similar legal process, including to law enforcement agencies, regulators, and courts in the United States and other countries where we operate;</li>
            <li>or with your consent.</li>
        </ul>
        <p>We encourage Business Partners to adopt and post privacy policies. However, their use of Personal Information obtained through Bixy is governed by their privacy policies and is not subject to our control.</p>
        <p>We may also disclose Non-Identifiable Information for the same reasons we might share Personal Information.</p>
        <h3 id="privacy-collection">How Bixy Collects Information</h3>
        <p>You provide us with your Personal Information when you register, subscribe, create an account, or redeem Bixy Points, or otherwise when you provide us with your Personal Information during your interaction with the Site or our Ads. We also collect Personal Information when you contact us online for customer service and other support using self-help tools, such as email, text, or by posting to an Online Community.</p>
        <p>We also receive Personal Information and other online and offline information from Other Sources. Bixy will use such information in accordance with applicable laws. Such information, when combined with Personal Information collected as provided in this Privacy Statement, will also be handled in accordance with this Privacy Statement. We also use cookies, tags, web beacons, local shared objects, files, tools and programs to keep records, store your preferences, improve our advertising, and collect Non-Identifiable Information, including Device Data and your interaction with the Site and our Business Partners' websites.</p>
        <p>When you access these pages or open email messages, we use Pixel Tags and Web Beacons to generate a notice of that action to us, or our Vendors. These tools allow us to measure response to our communications and improve the Site's pages and promotions.</p>
        <p>Device Data may be collected when your device interacts with the Site and Bixy, even if you are not logged into the Site using your device. If you have questions about the security and privacy settings of your mobile device, please refer to instructions from your mobile service provider or the manufacturer of your device to learn how to adjust your settings.</p>
        <h3 id="privacy-changes">How to make changes to your Information</h3>
        <p>You can access, modify and delete the information that you provided during account/profile creations by logging into your Bixy account and accessing the account settings section of the site. Users are able to delete their account by accessing the users setting area of Bixy. When an account it deleted all personally identifiable information is removed from our records.</p>
        <h3 id="privacy-assurances">Assurances</h3>
        <p>Bixy adheres to the Digital Advertising Alliance's Self-Regulatory Principles for Online Behavioral Advertising. For more information please visit <a target="_blank" href="www.aboutads.info">www.aboutads.info</a></p>
        <h3 id="privacy-social">Posting to online communities or social networks</h3>
        <p>Users are able to interact with various social media websites and online communities through Bixy. Any/All information posted to those sites can be seen, collected, used and sold by other parties besides Bixy. We cannot be responsible for the usage of this data.</p>
        <h3 id="privacy-security">Data security</h3>
        <p>Bixy utilizes security protocols to ensure that our users’ personal information is secure. We use industry standard encryption technology and secure hosting. For more information on data security you can visit <a target="_blank" href="https://aws.amazon.com/security/">https://aws.amazon.com/security/</a>.</p>
        <h3 id="privacy-third-party">Third Party Privacy Practices</h3>
        <p>This privacy policy only applies to Bixy. Other website may be accessible through Bixy, but these sites may have their own privacy statements.</p>
        <h3 id="privacy-children">Children's Policy</h3>
        <p>Our web sites are not directed to children under the age of 13, and as such we do not allow the registration by children under the age of 13. Bixy is designed and intended for individuals 18 and older. If we gain actual knowledge of the registered user under the age of 13 we will delete any and all information associated with that account. Also, we will not knowingly gather information (personally identifiable or other) for individuals under the age of 13.</p>
        <h3 id="privacy-contact">Contact Us</h3>
        <p>Users may contact us regarding privacy by email us at <a href="mailto:privacy@bixy.com">privacy@bixy.com</a>.</p>
        <h3 id="privacy-entity">Entity</h3>
        <p>Bixy <br>Email: <a href="mailto:privacy@bixy.com">privacy@bixy.com</a><br> Address: 826 North Fieldstone Drive<br> City: Lawrence<br> State: Kansas <br>Zip: 66049 <br>Country: US <br>Phone: 855.249.9776</p>
    </div>
</div>
            </div>
        </div>
    </div>
    <div id="forgotpassword" class="forgotpasswordpopup overflowpopup modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Password Reset</h4>
</div>
<div class="modal-body">
<form action="/business/authentication/forgotpassword" class="contact-form" method="post" name="ForgotPassword"><input name="__RequestVerificationToken" type="hidden" value="-d5u87oed8txnji3BCy3_o_IKj-5kNsvXSnwGCOigjEOrHUiZICFYn0S3tWKN3kOGYJJi1BDs2tTy1tph-kSSyhRj935Iq4g-Uv384h5CaU1" />        <div id="ResetPassword" style="display:block">
            <div class="form-group">
                <label>Your Email:</label>
                <input class="form-control" data-val="true" data-val-email="The Email Address field is not a valid e-mail address." data-val-required="The Email Address field is required." id="EmailAddress" name="EmailAddress" type="email" value="" />
            </div>
            <div class="form-btn">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                <input type="button" name="" class="btn" value="Reset Password" id="btnReset" onclick="javascript: Bixy.ResetPassword();">
            </div>
        </div>
        <div id="ResetPasswordSent" style="display:none">
            <p class="bumper-10">
                We've sent an e-mail <b></b> with a link to reset your password.
            </p>
            <p class="bumper-15">Please follow the steps in that message to reset your password.</p>
            <div class="fix"></div>
            <br /><br />
            <div class="fix"></div>
            <button type="button" class="btn" data-dismiss="modal" value="Done">Done</button>
            <div class="fix"></div>
        </div>
</form></div>
            </div>
        </div>
    </div>
    <div id="PasswordReset" class="PasswordResetpopup overflowpopup modal fade in" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

    <div class="modal-header">
        <button class="close" type="button" data-dismiss="modal" onclick="javascript: Bixy.Business.Landing.Close();">×</button>
        <h4 class="modal-title" id="lblTitle">Password Reset</h4>
    </div>
    <div class="modal-body">
<form action="/business/authentication/passwordreset" class="contact-form" method="post" name="PasswordReset"><input name="__RequestVerificationToken" type="hidden" value="_LzRtcbPNdf3JshbOqSCBjjf8YuVadF97tN6HsdRdvFjMdCRPvxWmfFV6VIeLtKj8Zrri1wClvuuzHbUpCud9GGhLAiGXbWgnB07phboZ9M1" />        <div id="ResetPassword" style="display:block">
            <div class="form-group">
                <label>Update Password</label>
                <input class="form-control" data-val="true" data-val-required="The New Password field is required." id="NewPassword" name="NewPassword" type="Password" value="" />
            </div>
            <div class="form-group">
                <label>Re-type Password</label>
                <input class="form-control" data-val="true" data-val-equalto="&#39;Confirm Password&#39; and &#39;New Password&#39; do not match." data-val-equalto-other="*.NewPassword" id="ConfirmPassword" name="ConfirmPassword" type="Password" value="" />
            </div>
            <div>
                <button class="btn" type="button" data-dismiss="modal" value="Reset Password" onclick="javascript: Bixy.Business.Landing.ResetPasswordUpdate();">Reset Password</button>
            </div>
        </div>
        <div id="ResetPasswordUpdated" style="display:none;">
            <p class="bumper-10">
                Your password has been reset. Close this box and log in with your new password to access your dashboard.
            </p>
            <br /><br />
            <button type="button" class="btn" data-dismiss="modal" value="CLOSE" onclick="javascript: Bixy.Business.Landing.Close();">CLOSE</button>
            <div class="fix"></div>
        </div>
</form></div>
            </div>
        </div>
    </div>

    <div id="AddNationalBrand" class="forgotpasswordpopup overflowpopup modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Overview Request</h4>
    <h6 class="modal-title">Let's get in touch. We'll get back to you in 48 hours.</h6>
</div>
<div class="modal-body">
<form action="/business/authentication/addnationalbrand" class="contact-form" method="post" name="addNationalBrand"><input name="__RequestVerificationToken" type="hidden" value="M2mTY7qBjGwZxrA-MtEikY9zKb0oX4IaLdSMrk_XERT6CQIMIQjRkoEOK9ZQQQhSeEEs_m95Rkom9p6cgxYl7exVOVjf0FQ0cFfeucFm2Lc1" />        <div id="NationalBrand" style="display:block">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Name:</label>
                        <input class="form-control" data-val="true" data-val-required="The Name field is required." id="Name" name="Name" type="text" value="" />
                    </div>
                    <div class="form-group">
                        <label>Company Name:</label>
                        <input class="form-control" data-val="true" data-val-required="The CompanyName field is required." id="CompanyName" name="CompanyName" type="text" value="" />
                    </div>
                    <div class="form-group">
                        <label>Email Address:</label>
                        <input class="form-control" data-val="true" data-val-email="The Email Address field is not a valid e-mail address." data-val-required="The Email Address field is required." id="EmailAddress" name="EmailAddress" type="text" value="" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Message:</label>
                        <textarea class="form-control" cols="20" data-val="true" data-val-required="The Message field is required." id="Message" name="Message" rows="2" type="text">
</textarea>
                    </div>
                    <div class="form-btn">
                        <input type="button" name="" class="btn" style="width:100%; margin:15px 0 0;" value="Submit" id="btnSubmit" onclick="javascript: Bixy.NationalBrand();">
                    </div>
                </div>
            </div>


        </div>
</form></div>

            </div>
        </div>
    </div>
    <footer id="colophon" class="site-footer py-3 bg-light-grey">
		<div class="container">
			<div id="footer-menus">
				<div class="row">
					<div class="col-3">
						<div class="menu-footer-1-container"><ul id="menu-footer-4" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="http://bixy.com/what-we-believe/">What We Believe</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="http://bixy.com/team/">Team</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="http://bixy.com/contact/">Contact</a></li>
</ul></div>					</div>
					<div class="col-3">
						<div class="menu-footer-2-container"><ul id="menu-footer-5" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-23 current_page_item menu-item-33"><a href="http://bixy.com/for-marketers/" aria-current="page">For Marketers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="http://bixy.com/for-media/">For Media</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="http://bixy.com/blog/">Blog</a></li>
</ul></div>					</div>
					<div class="col-3">
						<div class="menu-footer-3-container"><ul id="menu-footer-6" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-35"><a href="http://bixy.com/privacy-policy/">Privacy Policy</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="http://bixy.com/terms-of-use/">Terms Of Service</a></li>
</ul></div>					</div>
				</div>
			</div>
			<div id="footer-social-links" class="flex">
				<a target="_blank" href="https://facebook.com/teambixy" class="social-link flex justify-center align-center">
					<i class="fab fa-facebook-f"></i>
				</a>
				<a target="_blank" href="https://twitter.com/TeamBixy" class="social-link flex justify-center align-center">
					<i class="fab fa-twitter"></i>
				</a>
				<a target="_blank" href="https://www.instagram.com/teambixy/" class="social-link flex justify-center align-center">
					<i class="fab fa-instagram"></i>
				</a>
				<a target="_blank" href="https://www.youtube.com/channel/UCwji4DbpV15rPtgiWaioNgw" class="social-link flex justify-center align-center">
					<!-- <img src="http://bixy.com/wp-content/themes/bixy/images/youtube.png"/> -->
					<i class="fab fa-youtube"></i>
				</a>
			</div>
		</div>
	</footer>
</body>
</html>
<form action="/business/downloadoverviewlocalbrand" class="DLLocalBrand" method="post" name="downloadLocalBrand"><input name="__RequestVerificationToken" type="hidden" value="w6zQyGLBFcFJeZIYQN1LWnJjcsEj_crMGFYxuIBFU3HiYXikG_KsPlA_qHPltmZlg_OQEyAu4Ny-sEZM52Q5yInSRYNmw4bl0POs01NJ0Ic1" /></form><form action="/business/downloadoverviewlargebrand" class="DLLargeBrand" method="post" name="downloadLargeBrand"><input name="__RequestVerificationToken" type="hidden" value="JgCCUVZS0u8MMD4x8gh4oqCDfzUWNlj_wQ-_SMYHag_GPLEHuY66D5NCZKUBkf1fUOnL3ynK4uCk4EhHB5PMrFIGWl6OJ22RHrOjjmgvkng1" /></form><form action="/business/downloadpricing" class="DLDownloadPricing" method="post" name="DownloadPricing"><input name="__RequestVerificationToken" type="hidden" value="HKlTbDTEGhj9a3JRjgrCVjkOoYyQ-DvpFuThzuqIbFtmZ4qIaDWyfMX6rK4pOViYVXTRDxkZY2uC0YrV__S7zJzqjuQROx3BHwsP5K4z9501" /></form>
</body>
</html>
<script
src="https://code.jquery.com/jquery-3.4.1.min.js"
integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
crossorigin="anonymous"></script>
<script
src="<?php echo $main->js; ?>"></script>
