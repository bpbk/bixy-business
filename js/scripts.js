import $ from 'jquery';

$('.nav-tabs li a').on('click', function(e) {
  e.preventDefault()
  const type = $(this).html()
  $('.tab-pane').removeClass('active')
  $('#'+type).addClass('active')
  $('.nav-tabs li').removeClass('active')
  $(this).closest('li').addClass('active')
  $('.overviewtab').removeClass('login-active, signup-active')
  $('.overviewtab').addClass(type + '-active')
})
